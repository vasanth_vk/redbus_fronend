import React, { Component } from "react";

class NotFound extends Component {
    render() {
        return (
            <div
                className="d-flex justify-content-center align-items-center flex-column"
                style={{ marginTop: 50 }}
            >
                <h1 style={{ marginBottom: 50 }}>NOTHING HERE</h1>
                <i class="far fa-meh-rolling-eyes" style={{ fontSize: 90 }}></i>
            </div>
        );
    }
}

export default NotFound;
