import React, { Component } from "react";
import { Route, Switch } from "react-router";
import "./App.css";
import Awards from "./components/awards/awards";
import Navbar from "./components/header/navbar";
import Login from "./components/login/login";
import Offer from "./components/offer/offer";
import Promise from "./components/promise/promise";
import Safety from "./components/safety/safety";
import Search from "./components/search/search";
import SearchResults from "./components/searchResults/searchResults";
import Signup from "./components/signup/signup";
import { withRouter } from "react-router-dom";
// import axios from "axios";
// import { authenticateToken } from "./authenticate";
import NotFound from "./404";
// import SeatSelect from "./components/seatSelect/seatSelect";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginOpen: false,
            signupOpen: false,
            loggedIn: false,
            searchInfo: {},
            // stopScroll: true,
        };
    }
    handleLoginClick = (e) => {
        this.setState({
            loginOpen: !this.state.loginOpen,
            signupOpen: false,
        });
    };
    handleSignupClick = (e) => {
        this.setState({ signupOpen: !this.state.signupOpen, loginOpen: false });
    };

    handleLoginSuccess = (token, email) => {
        this.setState({ loginOpen: false, loggedIn: true });
        localStorage.setItem("rb_AT", JSON.stringify(token));
        // let Atoken = JSON.parse(localStorage.getItem("rb_AT"));
        // authenticateToken("").then((res) => console.log(res.data));
    };
    handleSignupSuccess = (token, email) => {
        localStorage.setItem("rb_AT", JSON.stringify(token));
        this.setState({ signupOpen: false, loggedIn: true });
    };
    handleLogout = () => {
        this.setState({
            loginOpen: false,
            signupOpen: false,
            loggedIn: false,
            searchInfo: {},
        });
    };
    handleSearch = (searched) => {
        this.props.history.push("/search");
        this.setState({ searchInfo: searched });
    };

    render() {
        return (
            <main>
                <Switch>
                    <Route path="/" exact>
                        <div
                            className="App"
                            // style={
                            //     this.state.stopScroll && { overflow: "hidden" }
                            // }
                        >
                            <Navbar
                                loginOpen={this.handleLoginClick}
                                loggedIn={this.state.loggedIn}
                                logout={this.handleLogout}
                            />

                            {this.state.loginOpen && (
                                <Login
                                    loginClose={this.handleLoginClick}
                                    loginOpen={this.handleLoginClick}
                                    signupOpen={this.handleSignupClick}
                                    loginSuccess={this.handleLoginSuccess}
                                />
                            )}
                            {this.state.signupOpen && (
                                <Signup
                                    signupClose={this.handleSignupClick}
                                    signupOpen={this.handleSignupClick}
                                    loginOpen={this.handleLoginClick}
                                    signupSuccess={this.handleSignupSuccess}
                                />
                            )}
                            <Search onSearch={this.handleSearch} />
                            <Offer />
                            <Safety />
                            <Promise />
                            <Awards />
                        </div>
                    </Route>
                    <Route path="/search">
                        <div className="App">
                            <Navbar
                                loginOpen={this.handleLoginClick}
                                loggedIn={this.state.loggedIn}
                                logout={this.handleLogout}
                            />
                            {this.state.loginOpen && (
                                <Login
                                    loginClose={this.handleLoginClick}
                                    loginOpen={this.handleLoginClick}
                                    signupOpen={this.handleSignupClick}
                                    loginSuccess={this.handleLoginSuccess}
                                />
                            )}
                            {this.state.signupOpen && (
                                <Signup
                                    signupClose={this.handleSignupClick}
                                    signupOpen={this.handleSignupClick}
                                    loginOpen={this.handleLoginClick}
                                    signupSuccess={this.handleSignupSuccess}
                                />
                            )}
                            {Object.keys(this.state.searchInfo).length === 0 ? (
                                <Route path="*" component={NotFound} />
                            ) : (
                                <SearchResults
                                    searchInfo={this.state.searchInfo}
                                />
                            )}
                        </div>
                    </Route>
                    <Route path="*" component={NotFound} />
                </Switch>
            </main>
        );
    }
}

export default withRouter(App);
