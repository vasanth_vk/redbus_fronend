import axios from "axios";

export const authenticateToken = async (token) => {
    try {
        let res = await axios.get(
            "https://intense-caverns-45038.herokuapp.com/login/authenticate",
            {
                headers: { authorization: `Bearer ${token}` },
                mode: "cors",
            }
        );
        return res;
    } catch (err) {
        console.log(err);
    }
};
