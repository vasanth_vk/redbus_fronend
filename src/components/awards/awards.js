import React, { Component } from "react";
import "./awards.css";
class Awards extends Component {
    render() {
        return (
            <div className="awards-section">
                <h2>AWARDS & RECOGNITION</h2>
                <div className="awards-container">
                    <a
                        className="awards"
                        href="https://www.business-standard.com/article/companies/bs-annual-awards-saluting-the-spirit-of-entrepreneurship-114033100015_1.html"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <img
                            className="award-img"
                            alt="award"
                            src="https://s2.rdbuz.com/web/images/home/awards/Business_Standard1.png"
                        ></img>
                        <p className="award-desc">Most Innovative Company</p>
                    </a>
                    <a
                        className="awards"
                        href="https://thebrandtrustreport.wordpress.com/tag/redbus-in/"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <img
                            className="award-img"
                            alt="award"
                            src="https://s1.rdbuz.com/web/images/home/awards/Brand_Trust_Report.png"
                        ></img>
                        <p className="award-desc">Most Trusted Brand</p>
                    </a>
                    <a
                        className="awards"
                        href="https://eyefortravelblog.blogspot.com/2014/04/winners-of-mobile-innovation-in-travel.html"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <img
                            className="award-img"
                            alt="award"
                            src="https://s3.rdbuz.com/web/images/home/awards/Eye_for_Travel1.png"
                        ></img>
                        <p className="award-desc">Mobile Innovation Award</p>
                    </a>
                </div>
            </div>
        );
    }
}
export default Awards;
