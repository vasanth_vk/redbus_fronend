import React, { Component } from "react";
import "./navbar.css";
class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openProfile: false,
        };
    }
    handleProfile = (e) => {
        this.setState({ openProfile: !this.state.openProfile });
    };
    render() {
        return (
            <nav>
                <div className="nav-start">
                    <a className="redbus-logo" href="/">
                        REDBUS
                    </a>
                    <ul className="nav-start-ul">
                        <li>
                            <a href="/">BUS TICKETS</a>
                        </li>
                        <li>
                            <a href="/">rPoolNew</a>
                        </li>
                        <li>
                            <a href="/">BUS HIRE</a>
                        </li>
                    </ul>
                </div>
                <div className="nav-end">
                    <ul className="nav-end-ul">
                        <li>
                            <a href="/">Help</a>
                        </li>
                        <li>
                            <a href="/">Print/SMS Ticket</a>
                        </li>
                        <li>
                            <a href="/">Cancel Ticket</a>
                        </li>
                    </ul>
                    <div className="profile" onClick={this.handleProfile}>
                        <svg
                            ariahidden="true"
                            focusable="false"
                            dataprefix="far"
                            dataicon="user-circle"
                            className="svg-inline--fa fa-user-circle fa-w-16"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 496 512"
                        >
                            <path
                                fill="#ffff"
                                d="M248 104c-53 0-96 43-96 96s43 96 96 96 96-43 96-96-43-96-96-96zm0 144c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm0-240C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm0 448c-49.7 0-95.1-18.3-130.1-48.4 14.9-23 40.4-38.6 69.6-39.5 20.8 6.4 40.6 9.6 60.5 9.6s39.7-3.1 60.5-9.6c29.2 1 54.7 16.5 69.6 39.5-35 30.1-80.4 48.4-130.1 48.4zm162.7-84.1c-24.4-31.4-62.1-51.9-105.1-51.9-10.2 0-26 9.6-57.6 9.6-31.5 0-47.4-9.6-57.6-9.6-42.9 0-80.6 20.5-105.1 51.9C61.9 339.2 48 299.2 48 256c0-110.3 89.7-200 200-200s200 89.7 200 200c0 43.2-13.9 83.2-37.3 115.9z"
                            ></path>
                        </svg>
                        <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fas"
                            data-icon="caret-down"
                            className="svg-inline--fa fa-caret-down fa-w-10"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                        >
                            <path
                                fill="#fff"
                                d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"
                            ></path>
                        </svg>

                        {this.state.openProfile ? (
                            this.props.loggedIn ? (
                                <div className="login-drop-down">
                                    <button
                                        type="button"
                                        className="btn btn-light drop-down-item"
                                    >
                                        Mytrips
                                    </button>
                                    <button
                                        type="button"
                                        className="btn btn-light drop-down-item"
                                        onClick={this.props.logout}
                                    >
                                        Logout
                                    </button>
                                    <button
                                        type="button"
                                        className="btn btn-light drop-down-item"
                                    >
                                        Mytrips
                                    </button>
                                </div>
                            ) : (
                                <button
                                    type="button"
                                    className="btn btn-light drop-down"
                                    onClick={this.props.loginOpen}
                                >
                                    Signin/signup
                                </button>
                            )
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navbar;
