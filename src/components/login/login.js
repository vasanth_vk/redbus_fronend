import axios from "axios";
import React, { Component } from "react";
import "./login.css";
import isEmail from "validator/lib/isEmail";
import Slide from "@material-ui/core/Slide";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            successMessage: "",
            isLoading: false,
        };
    }
    handleLoginSubmit = (event) => {
        this.setState({
            errorMessage: "",
            successMessage: "",
            isLoading: true,
        });

        event.preventDefault();
        const data = new FormData(event.target);
        let user = {};
        for (var [key, value] of data.entries()) {
            user[key] = value;
        }
        if (isEmail(user.email)) {
            axios({
                method: "post",
                url: "https://intense-caverns-45038.herokuapp.com/login",
                data: user,
            })
                .then((res) => {
                    if (res.data.message) {
                        this.setState({ errorMessage: res.data.message });
                        this.setState({ isLoading: false });
                    }
                    if (res.data.accessToken) {
                        this.setState({ successMessage: "Login successful" });
                        this.props.loginSuccess(
                            res.data.accessToken,
                            user.email
                        );
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            this.setState({ errorMessage: "Enter valid email" });
            this.setState({ isLoading: false });
        }
    };

    render() {
        return (
            <div className="login-section">
                <Slide direction="up" in={true} mountOnEnter unmountOnExit>
                    <div className="login-container">
                        <div className="left-side-image">
                            <p>Unlock the Smarter Way to Travel</p>
                        </div>
                        <div className="signin-module">
                            <div className="logo"></div>

                            <p>
                                Sign in to avail exciting discounts and
                                cashbacks!!
                            </p>
                            <form
                                className="register-form"
                                onSubmit={(e) => this.handleLoginSubmit(e)}
                            >
                                <div className="invalid-feedback d-block">
                                    {this.state.errorMessage}
                                </div>
                                <div className="valid-feedback d-block">
                                    {this.state.successMessage}
                                </div>
                                <div className="form-group">
                                    {/* <label htmlFor="email">Email address</label> */}
                                    <div className="input-container">
                                        <i className="far fa-envelope-open"></i>
                                        <input
                                            type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Enter email"
                                            required
                                        />
                                    </div>

                                    {/* <label htmlFor="password">Password</label> */}
                                    <div className="input-container">
                                        <i className="far fa-eye"></i>
                                        <input
                                            type="password"
                                            className="form-control"
                                            id="password"
                                            placeholder="Password"
                                            name="password"
                                            required
                                            style={{ marginTop: 10 }}
                                        />
                                    </div>
                                    {this.state.isLoading ? (
                                        <button
                                            type="button"
                                            className="btn btn-danger btn-block"
                                            id="loading-btn"
                                            style={{
                                                marginTop: 10,
                                                width: "100%",
                                            }}
                                        >
                                            Logging in{" "}
                                            <i
                                                className="fas fa-circle-notch"
                                                id="loader"
                                            ></i>
                                        </button>
                                    ) : (
                                        <button
                                            type="submit"
                                            className="btn btn-danger btn-block"
                                            id="register-submit"
                                            style={{
                                                marginTop: 10,
                                                width: "100%",
                                            }}
                                        >
                                            Login
                                        </button>
                                    )}

                                    <div style={{ paddingTop: 10 }}>
                                        <span>
                                            New user{" "}
                                            <i className="fas fa-angle-double-down"></i>
                                        </span>
                                        <button
                                            type="button"
                                            className="btn btn-danger btn-block"
                                            style={{
                                                marginTop: 10,
                                                width: "100%",
                                            }}
                                            onClick={(e) =>
                                                this.props.signupOpen(e)
                                            }
                                        >
                                            Signup
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <span className="terms">
                                By signing up, you agree to our{" "}
                                <a
                                    href="redbus.in/info/termscondition"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    Terms & Conditions
                                </a>
                                and{" "}
                                <a
                                    href="https://www.redbus.in/info/privacypolicy"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    Privacy Policy
                                </a>
                            </span>
                            <button
                                className="btn close-btn"
                                onClick={(e) => this.props.loginClose(e)}
                            >
                                x
                            </button>
                            {/* <i className="fas fa-sign-in-alt"></i> */}
                        </div>
                    </div>
                </Slide>
            </div>
        );
    }
}

export default Login;
