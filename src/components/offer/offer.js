import React, { Component } from "react";
import "./offer.css";
class Offer extends Component {
    render() {
        return (
            <div id="carousel-wrapper" className="carousel-wrapper">
                <ul className="carousel-container" id="offer_container">
                    <li className="offer-slide-item">
                        <p className="offer-det">AMAZON pay offer</p>
                        <img
                            src="https://s1.rdbuz.com/images/MobileOffers/amazon/offertile..png"
                            alt=""
                        />
                        <p className="offer-code">
                            Win Rs 10 to Rs 300 on minimum purchase ofRs 300.
                        </p>
                    </li>
                    <li className="  offer-slide-item ">
                        <p className="offer-det">
                            Save up to Rs 150 on bus tickets
                        </p>
                        <img
                            src="https://st.redbus.in/Images/INDOFFER/safetyplus/274_147.png"
                            alt=""
                        />
                        <p className="offer-code">Use code FIRST</p>
                    </li>
                    <li className="  offer-slide-item ">
                        <p className="offer-det">redBus Cares</p>
                        <img
                            src="https://st.redbus.in/Images/INDOFFER/COVID-19/desktopdonate.png"
                            alt=""
                        />
                        <p className="offer-code">
                            A Little Care goes a long way
                        </p>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Offer;
