import React, { Component } from "react";
import "./promise.css";

class Promise extends Component {
    render() {
        return (
            <div id="advantage_div">
                <section>
                    <div className="aw main-body">
                        <div className="ah heading-1 main-header-family rest1 animate">
                            <div className="dib">
                                <img
                                    src="//s1.rdbuz.com/web/images/home/promise.png"
                                    height="100"
                                    alt=""
                                />
                            </div>
                            <div className="promise-head-main">
                                {" "}
                                We promise to deliver
                            </div>
                        </div>
                        <div className="ad rest1 animate"></div>
                        <div
                            className="clearfix aa our-promise-blocks"
                            id="advantage"
                        >
                            <div className="fl rest1 appear-first aa-25 animate">
                                <div className="imgCont rest1 animate">
                                    <img
                                        src="//s3.rdbuz.com/Images/safety/srp/safety.svg"
                                        height="90"
                                        alt=""
                                    />
                                </div>
                                <div className="tilleBlock rest1 animate">
                                    SAFETY+
                                </div>
                                <div className="second-level-heading descCont rest1 animate">
                                    With Safety+ we have brought in a set of
                                    measures like Sanitized buses, mandatory
                                    masks etc. to ensure you travel safely.
                                </div>
                            </div>
                            <div className="fl rest1 appear-second aa-25 animate">
                                <div className="imgCont rest1 animate">
                                    <img
                                        src="//s1.rdbuz.com/web/images/home/customer_care.png"
                                        height="100"
                                        alt=""
                                    />
                                </div>
                                <div className="tilleBlock rest1 animate">
                                    SUPERIOR CUSTOMER SERVICE
                                </div>
                                <div className="second-level-heading descCont rest1 animate">
                                    We put our experience and relationships to
                                    good use and are available to solve your
                                    travel issues.
                                </div>
                            </div>
                            <div className="fl rest1 appear-third aa-25 animate">
                                <div className="imgCont rest1 animate">
                                    <img
                                        src="//s1.rdbuz.com/web/images/home/lowest_Fare.png"
                                        height="90"
                                        alt=""
                                    />
                                </div>
                                <div className="tilleBlock rest1 animate">
                                    LOWEST PRICES
                                </div>
                                <div className="second-level-heading descCont rest1 animate">
                                    We always give you the lowest price with the
                                    best partner offers.
                                </div>
                            </div>
                            <div className="fl rest1 appear-fourth aa-25 animate">
                                <div className="imgCont rest1 animate">
                                    <img
                                        src="//s2.rdbuz.com/web/images/home/benefits.png"
                                        height="90"
                                        alt=""
                                    />
                                </div>
                                <div className="tilleBlock rest1 animate">
                                    UNMATCHED BENEFITS
                                </div>
                                <div className="second-level-heading descCont rest1 animate">
                                    We take care of your travel beyond ticketing
                                    by providing you with innovative and unique
                                    benefits.
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Promise;
