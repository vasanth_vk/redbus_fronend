import React, { Component } from "react";
import "./safety.css";

class Safety extends Component {
    render() {
        return (
            <div className="HeroMain">
                <div className="header">
                    <div className="img2_Hero"></div>

                    <span className="fl">
                        <div className="Title_hero">Safety+</div>
                        <div className="subtext_hero">
                            Opt to Travel Safe with redBus
                            <a
                                className="know-more-anchor"
                                href="https://www.redbus.in/SafetyPlus"
                                target="_blank"
                                rel="noreferrer"
                            >
                                Know more
                            </a>
                        </div>
                    </span>
                    <span className="fr"></span>
                </div>
                <div className="subHeader_hero">
                    <span className="img3_hero"></span>
                    <span className="Herotext1">Look for buses with </span>
                    <span className="img4_Hero"></span>
                    <span className="text">
                        tag while booking your journey,{" "}
                    </span>
                </div>

                <div className="info_hero">
                    <div className="titl_hero"> Sanitized Bus </div>
                    <div className="value_hero"> </div>
                    <div className="text_hero">
                        All Safety+ buses are sanitized and disinfected before
                        and after every trip.{" "}
                    </div>
                </div>
                <div className="info_hero2">
                    <div className="titl_hero"> Mandatory masks </div>
                    <div className="value_hero"> </div>
                    <div className="text_hero">
                        Proper masks are mandatory for all passengers and bus
                        staff.
                    </div>
                </div>
                <div className="info_hero2">
                    <div className="titl_hero"> Thermal Screening </div>
                    <div className="value_hero"> </div>
                    <div className="text_hero">
                        All passengers will undergo thermal screening.
                        Temperature checks for bus drivers and service staff are
                        done before every trip.{" "}
                    </div>
                </div>
            </div>
        );
    }
}

export default Safety;
