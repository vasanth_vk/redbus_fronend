export const cities = [
    { city: "Bengaluru", id: 1 },
    { city: "Delhi", id: 2 },
    { city: "Mumbai", id: 3 },
    { city: "Hyderabad", id: 4 },
    { city: "Chennai", id: 5 },
    { city: "Pune", id: 6 },
    { city: "Goa", id: 7 },
];

export const operators = [
    {
        id: 1,
        operator: "SRS Travels",
    },
    {
        id: 2,
        operator: "Mahadev Travels",
    },
    {
        id: 3,
        operator: "Jakhar Travels",
    },
    {
        id: 4,
        operator: "M R Travels",
    },
    {
        id: 5,
        operator: "SAS Travels",
    },
    {
        id: 6,
        operator: "Meera Tours & Travels",
    },
    {
        id: 7,
        operator: "Khushi Tourist",
    },
    {
        id: 8,
        operator: "Jkk Travels",
    },
    {
        id: 9,
        operator: "Morning Star Travels",
    },
    {
        id: 10,
        operator: "Big Bus",
    },
    {
        id: 11,
        operator: "Varsha Travels",
    },
    {
        id: 12,
        operator: "Simhapuri Travels",
    },
    {
        id: 13,
        operator: "Intercity travels",
    },
    {
        id: 14,
        operator: "Seabird Tourists",
    },
];

export const busesData = [
    {
        id: 1,
        operatorID: 1,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 2,
        operatorID: 1,
        amenitiesID: [1, 2, 3, 4],
    },
    {
        id: 3,
        operatorID: 4,
        amenitiesID: [4, 5, 6, 7],
    },
    {
        id: 4,
        operatorID: 5,
        amenitiesID: [2, 4, 6, 8],
    },
    {
        id: 5,
        operatorID: 6,
        amenitiesID: [1, 2, 3, 6, 8],
    },
    {
        id: 6,
        operatorID: 7,
        amenitiesID: [9, 8, 5, 3, 1],
    },
    {
        id: 7,
        operatorID: 8,
        amenitiesID: [1, 2, 3, 4, 7, 2],
    },
    {
        id: 8,
        operatorID: 1,
        amenitiesID: [8, 9, 4, 3, 1],
    },
    {
        id: 9,
        operatorID: 2,
        amenitiesID: [1, 3, 5, 7, 9],
    },
    {
        id: 10,
        operatorID: 3,
        amenitiesID: [1, 2, 3, 5, 7, 9],
    },
    {
        id: 11,
        operatorID: 11,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 12,
        operatorID: 1,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 13,
        operatorID: 2,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 14,
        operatorID: 5,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 15,
        operatorID: 4,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 16,
        operatorID: 9,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 17,
        operatorID: 10,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 18,
        operatorID: 11,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 19,
        operatorID: 11,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 20,
        operatorID: 11,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 21,
        operatorID: 12,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 22,
        operatorID: 12,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 23,
        operatorID: 13,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 24,
        operatorID: 14,
        amenitiesID: [1, 2, 3],
    },
    {
        id: 25,
        operatorID: 14,
        amenitiesID: [1, 2, 3],
    },
];
// M-ticket Supported
// Regular temperature checks

// No blankets/linens

// Reading Light

export const amenities = [
    { id: 1, amenity: "Staff with masks" },
    { id: 2, amenity: "Hand Sanitisers Provided" },
    { id: 3, amenity: "Deep Cleaned Buses" },
    { id: 4, amenity: "Emergency Contact Number" },
    { id: 5, amenity: "Track My Bus" },
    { id: 6, amenity: "Charging Point" },
    { id: 7, amenity: "Water Bottle" },
    { id: 8, amenity: "Reading Light" },
    { id: 9, amenity: "Wifi" },
];

export const routesData = [
    {
        id: 1,
        sourceCityID: 1,
        destinationCityID: 5,
        busID: 1,
        operator: "SRS Travels",
        amenities: ["Wifi", "Staff with masks", "Hand Sanitisers Provided"],
        departureTime: "23:15",
        arrivalTime: "05:30",
        duration: "5h 20m",
        departureDate: "2021-05-28",
        arrivalDate: "2021-05-29",
        fare: 649,
        seats: 24,
    },
    {
        id: 2,
        sourceCityID: 1,
        destinationCityID: 5,
        busID: 2,
        operator: "Mahadev Travels",
        amenities: [
            "Wifi",
            "Staff with masks",
            "Hand Sanitisers Provided",
            "Water Bottle",
        ],
        departureTime: "12:15",
        arrivalTime: "19:25",
        duration: "5h 20m",
        departureDate: "2021-05-28",
        arrivalDate: "2021-05-28",
        fare: 650,
        seats: 32,
    },
    {
        id: 3,
        sourceCityID: 1,
        destinationCityID: 5,
        busID: 3,
        operator: "Jakhar Travels",
        amenities: [
            "Wifi",
            "Staff with masks",
            "Hand Sanitisers Provided",
            "Charging Point",
        ],
        departureTime: "07:00",
        arrivalTime: "13:35",
        duration: "5h 20m",
        departureDate: "2021-05-28",
        arrivalDate: "2021-05-28",
        fare: 1250,
        seats: 44,
    },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
    // {
    //     id: 1,
    //     sourceCityID: 1,
    //     destinationCityID: 5,
    //     busID: 1,
    //     departureTime: "23:15",
    //     arrivalTime: "05:30",
    //     departureDate: "27-05-2021",
    //     arrivalDate: "28-05-2021",
    //     fare: 649,
    // },
];
