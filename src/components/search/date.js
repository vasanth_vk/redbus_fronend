import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        flexWrap: "wrap",
        background: "white",
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
        height: 40,
        textDecoration: "none",
    },
    "&&&:before": {
        borderBottom: "none",
    },
    "&&:after": {
        borderBottom: "none",
    },
}));

export default function DatePickers(props) {
    const classes = useStyles();
    let today = new Date();
    let todaysDate =
        today.getFullYear() +
        "-" +
        ("0" + (today.getMonth() + 1)).slice(-2) +
        "-" +
        ("0" + today.getDate()).slice(-2);

    return (
        <form className={classes.container} noValidate>
            <TextField
                id="date"
                label=""
                type="date"
                defaultValue={todaysDate}
                className={classes.textField}
                InputLabelProps={{
                    shrink: true,
                    required: true,
                }}
                onChange={(e) => props.handleDateChange(e)}
                InputProps={{
                    disableUnderline: true,
                    inputProps: { min: todaysDate },
                }}
            />
        </form>
    );
}
