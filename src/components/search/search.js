import React, { Component } from "react";
import "./search.css";
import DatePickers from "./date";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { cities } from "./citiesObj";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fromValue: "",
            toValue: "",
            dateValue: "",
        };
    }

    handleDateChange = (e) => {
        this.setState({ dateValue: e.target.value });
    };

    handleSearch = (e) => {
        if (this.state.fromValue && this.state.toValue) {
            this.props.onSearch(this.state);
        }
    };
    componentDidMount() {
        let date = new Date();
        let today =
            date.getFullYear() +
            "-" +
            ("0" + (date.getMonth() + 1)).slice(-2) +
            "-" +
            ("0" + date.getDate()).slice(-2);
        this.setState({ dateValue: today });
    }

    render() {
        return (
            <div className="search-container">
                <div className="search-box">
                    <Autocomplete
                        freeSolo
                        selectOnFocus
                        clearOnBlur
                        handleHomeEndKeys
                        id="city-search-input"
                        disableClearable
                        onChange={(event, newValue) => {
                            this.setState({
                                fromValue: newValue,
                            });
                        }}
                        options={cities
                            .filter((ele) => ele.city !== this.state.toValue)
                            .map((ele) => ele.city)}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                id="city-text-field"
                                label="From"
                                margin="normal"
                                variant="outlined"
                                InputProps={{
                                    ...params.InputProps,
                                    type: "search",
                                }}
                            />
                        )}
                    />
                    <Autocomplete
                        freeSolo
                        selectOnFocus
                        clearOnBlur
                        handleHomeEndKeys
                        id="city-search-input"
                        disableClearable
                        onChange={(event, newValue) => {
                            this.setState({
                                toValue: newValue,
                            });
                        }}
                        options={cities
                            .filter((ele) => ele.city !== this.state.fromValue)
                            .map((ele) => ele.city)}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                id="city-text-field"
                                label="To"
                                margin="normal"
                                variant="outlined"
                                InputProps={{
                                    ...params.InputProps,
                                    type: "search",
                                }}
                            />
                        )}
                    />
                    <DatePickers handleDateChange={this.handleDateChange} />
                    <button
                        className="search-btn"
                        onClick={(e) => this.handleSearch(e)}
                    >
                        Search Buses
                    </button>
                </div>
            </div>
        );
    }
}

export default Search;
