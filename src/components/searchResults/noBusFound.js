import React, { Component } from "react";
class Nobus extends Component {
    render() {
        return (
            <div className="nobus-container d-flex flex-column justify-content-center align-items-center">
                <img
                    className="nobus-img"
                    src="https://www.redbus.in/images/no_bus.png"
                    alt="busnotfound"
                ></img>
                <h3>Oops! No buses found.</h3>
            </div>
        );
    }
}

export default Nobus;
