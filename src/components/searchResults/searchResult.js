import moment from "moment";
import React, { Component } from "react";
import SeatSelect from "../seatSelect/seatSelect";
import "./searchResult.css";
class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            travelDuration: "",
            showSeats: false,
        };
    }

    handleViewSeats = (seatInfo) => {
        this.setState({ showSeats: !this.state.showSeats });
    };
    componentDidMount() {
        let a = moment(this.props.busInfo.departureTime, "HH:mm:ss");
        let b = moment(this.props.busInfo.arrivalTime, "HH:mm:ss");
        let hr = b.diff(a, "hours");
        if (hr < 0) {
            hr += 24;
        }
        let min = a.diff(b, "minutes") % 60;
        if (min < 0) {
            min += 60;
        }
        let diff = `${hr}h:${min}m`;
        this.setState({ travelDuration: diff });
    }
    render() {
        return (
            <div>
                <div className="search-results-container">
                    <div className="bus-info-container">
                        <h5 className="bus-operator">
                            {this.props.busInfo.operator}
                        </h5>
                        <div className="bus-amenities">
                            {this.props.busInfo.amenity.join(" | ")}
                        </div>
                    </div>
                    <div className="departure">
                        <h5 className="dep-time">
                            {this.props.busInfo.departureTime}
                        </h5>
                        <span className="dep-from">Madiwala</span>
                    </div>
                    <div className="duration-container">
                        <span className="duration">
                            {this.state.travelDuration}
                        </span>
                    </div>
                    <div className="arrival">
                        <h5 className="arr-time">
                            {this.props.busInfo.arrivalTime}
                        </h5>
                        <span className="arr-to">Madiwala</span>
                    </div>
                    <div className="fare-container">
                        <span className="fare">
                            INR {this.props.busInfo.fare}
                        </span>
                    </div>
                    <div className="seat-info">
                        <span className="seats-available">
                            {this.props.busInfo.seat
                                ? this.props.busInfo.seat.filter(
                                      (ele) => !ele.seatBooked
                                  ).length
                                : "0"}{" "}
                            Seats available
                        </span>
                    </div>
                    <button
                        className="view-seats btn btn-danger"
                        onClick={this.handleViewSeats}
                    >
                        VIEW SEATS
                    </button>
                </div>
                {this.state.showSeats && (
                    <SeatSelect seats={this.props.busInfo.seat}></SeatSelect>
                )}
            </div>
        );
    }
}
export default SearchResult;
