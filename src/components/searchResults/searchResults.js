import axios from "axios";
import moment from "moment";
import React, { Component } from "react";
// import { cities, routesData } from "../search/citiesObj";
import Nobus from "./noBusFound";
import SearchResult from "./searchResult";
import "./searchResults.css";

class SearchResults extends Component {
    constructor(props) {
        super(props);
        this.state = {
            availableBuses: [],
        };
    }

    handleDateChange = () => {
        let today = moment(this.props.searchInfo.dateValue);
        let tomorrow = today.add(1, "days");
        console.log(moment(tomorrow).format("YYYY-MM-DD"));
    };
    componentDidMount() {
        let data = {
            source: this.props.searchInfo.fromValue.trim(),
            destination: this.props.searchInfo.toValue,
            departureDate: this.props.searchInfo.dateValue,
        };
        console.log(data);
        axios({
            method: "post",
            url: "https://redbus-backend-vk.herokuapp.com/busRoute",
            data: data,
        })
            .then((res) => {
                console.log(res.data);
                this.setState({ availableBuses: res.data });
            })
            .catch((err) => {
                console.log(err);
            });
    }
    render() {
        return (
            <div className="search-results-section">
                <div className="search-header">
                    <span className="src">
                        {this.props.searchInfo.fromValue}
                    </span>
                    <i className="fas fa-arrow-right"></i>
                    <span className="dest">
                        {this.props.searchInfo.toValue}
                    </span>
                    <div className="date-container">
                        <button
                            className="prev-date btn"
                            onClick={this.handleDateChange}
                        >
                            <i className="fas fa-less-than"></i>
                        </button>
                        <span>{this.props.searchInfo.dateValue}</span>
                        <button
                            className="next-date btn"
                            onClick={this.handleDateChange}
                        >
                            <i className="fas fa-greater-than"></i>
                        </button>
                    </div>
                </div>
                {this.state.availableBuses.length === 0 ? <Nobus /> : <></>}
                {this.state.availableBuses.map((bus) => {
                    return <SearchResult key={bus.id} busInfo={bus} />;
                })}
            </div>
        );
    }
}
export default SearchResults;
