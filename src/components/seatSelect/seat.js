import React, { Component } from "react";
import "./seatSelect.css";
class Seat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            seatClicked: false,
        };
    }
    render() {
        return (
            <div
                className="seat"
                id={this.state.seatClicked && "seat-selected"}
            >
                <button
                    className={
                        this.props.seatInfo.seatBooked
                            ? "booked btn d-flex align-items-center flex-column"
                            : "btn d-flex align-items-center flex-column"
                    }
                    style={{ margin: 0, padding: 0 }}
                    onClick={(e) => {
                        this.setState({ seatClicked: !this.state.seatClicked });
                        e.stopPropagation();
                        this.props.seatClick(this.props.seatInfo);
                    }}
                >
                    <span className="seat-no">{this.props.seatInfo.seat}</span>
                    <i
                        className={
                            this.props.seatInfo.seatBooked
                                ? "fas fa-chair booked"
                                : "fas fa-chair"
                        }
                    ></i>
                </button>
            </div>
        );
    }
}

export default Seat;
