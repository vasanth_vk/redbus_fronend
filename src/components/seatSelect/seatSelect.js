import React, { Component } from "react";
import Seat from "./seat";
import "./seatSelect.css";
class SeatSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            upperLeftSeats: [],
            upperRightSeats: [],
            lowerRightSeats: [],
            lowerLeftSeats: [],
            seatsSelected: [],
        };
    }
    componentDidMount() {
        let noOfseats = Math.floor(this.props.seats.length / 4);
        let upperLeft = this.props.seats.slice(0, noOfseats);
        let upperRight = this.props.seats.slice(noOfseats, noOfseats * 2);
        let lowerLeft = this.props.seats.slice(noOfseats * 2, noOfseats * 3);
        let lowerRight = this.props.seats.slice(noOfseats * 3, noOfseats * 4);
        this.setState({
            upperLeftSeats: upperLeft,
            upperRightSeats: upperRight,
            lowerRightSeats: lowerRight,
            lowerLeftSeats: lowerLeft,
        });
    }
    handleSeatSelection = (seatSelected) => {
        if (this.state.seatsSelected.length < 6) {
            this.setState({
                seatsSelected: [...this.state.seatsSelected, seatSelected],
            });
        } else {
            alert("The maximum number of seats that can be selected is 6");
        }
    };
    render() {
        return (
            <div className="seat-select-section" style={{ padding: 10 }}>
                <div className="seat-select-container">
                    <div className="upper-seats">
                        <div className="upper-left-seats">
                            {this.state.upperLeftSeats.map((seat) => {
                                return (
                                    <Seat
                                        key={seat.id}
                                        seatInfo={seat}
                                        seatClick={this.handleSeatSelection}
                                    />
                                );
                            })}
                        </div>
                        <div className="upper-right-seats">
                            {this.state.upperRightSeats.map((seat) => {
                                return (
                                    <Seat
                                        key={seat.id}
                                        seatInfo={seat}
                                        seatClick={this.handleSeatSelection}
                                    />
                                );
                            })}
                        </div>
                    </div>
                    <div className="lower-seats">
                        <div className="lower-left-seats">
                            {this.state.lowerLeftSeats.map((seat) => {
                                return (
                                    <Seat
                                        key={seat.id}
                                        seatInfo={seat}
                                        seatClick={this.handleSeatSelection}
                                    />
                                );
                            })}
                        </div>
                        <div className="lower-right-seats">
                            {this.state.lowerRightSeats.map((seat) => {
                                return (
                                    <Seat
                                        key={seat.id}
                                        seatInfo={seat}
                                        seatClick={this.handleSeatSelection}
                                    />
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SeatSelect;
