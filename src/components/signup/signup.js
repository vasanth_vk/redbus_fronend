import axios from "axios";
import React, { Component } from "react";
import isEmail from "validator/lib/isEmail";
import "./signup.css";

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            successMessage: "",
            isLoading: false,
        };
    }

    handleSignupSubmit = (event) => {
        this.setState({
            errorMessage: "",
            successMessage: "",
            isLoading: true,
        });

        event.preventDefault();
        const data = new FormData(event.target);
        let user = {};
        for (var [key, value] of data.entries()) {
            user[key] = value;
        }
        if (isEmail(user.email)) {
            axios({
                method: "post",
                url: "https://intense-caverns-45038.herokuapp.com/register",
                data: user,
            })
                .then((res) => {
                    if (res.data.message === "userExists") {
                        this.setState({ errorMessage: "User already exists" });
                    } else if (res.data.message === "userCreated") {
                        this.setState({ successMessage: "User created" });
                        this.props.signupSuccess(
                            res.data.accessToken,
                            user.email
                        );
                        console.log(res.data);
                    }
                    this.setState({ isLoading: false });
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            this.setState({ errorMessage: "Enter valid details" });
            this.setState({ isLoading: false });
        }
    };

    render() {
        return (
            <div className="login-section">
                <div className="login-container">
                    <div className="left-side-image">
                        <p>Unlock the Smarter Way to Travel</p>
                    </div>
                    <div className="signin-module">
                        <div className="logo"></div>

                        <form
                            className="register-form"
                            onSubmit={(e) => this.handleSignupSubmit(e)}
                        >
                            <div className="invalid-feedback d-block">
                                {this.state.errorMessage}
                            </div>
                            <div className="valid-feedback d-block">
                                {this.state.successMessage}
                            </div>
                            <div className="form-group">
                                {/* <label htmlFor="email">Email address</label> */}

                                <div className="input-container">
                                    <i className="far fa-envelope-open"></i>
                                    <input
                                        type="firstname"
                                        className="form-control"
                                        id="firstname"
                                        name="firstName"
                                        placeholder="Enter firstname"
                                        required
                                    />
                                </div>

                                <div className="input-container">
                                    <i className="far fa-envelope-open"></i>
                                    <input
                                        type="lastname"
                                        className="form-control"
                                        id="lastname"
                                        name="lastName"
                                        placeholder="Enter lastname"
                                        required
                                    />
                                </div>

                                <div className="input-container">
                                    <i className="far fa-envelope-open"></i>
                                    <input
                                        type="email"
                                        className="form-control"
                                        id="email"
                                        name="email"
                                        placeholder="Enter email"
                                        required
                                    />
                                </div>

                                {/* <label htmlFor="password">Password</label> */}
                                <div className="input-container">
                                    <i className="far fa-eye"></i>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        placeholder="Password"
                                        name="password"
                                        required
                                        style={{ marginTop: 10 }}
                                    />
                                </div>

                                {this.state.isLoading ? (
                                    <button
                                        type="button"
                                        className="btn btn-danger btn-block"
                                        id="loading-btn"
                                        style={{ marginTop: 10, width: "100%" }}
                                    >
                                        Signing up{" "}
                                        <i
                                            className="fas fa-circle-notch"
                                            id="loader"
                                        ></i>
                                    </button>
                                ) : (
                                    <button
                                        type="submit"
                                        className="btn btn-danger btn-block"
                                        id="register-submit"
                                        style={{ marginTop: 10, width: "100%" }}
                                    >
                                        Signup
                                    </button>
                                )}

                                <div style={{ paddingTop: 10 }}>
                                    <span>
                                        Already have an account{" "}
                                        <i className="fas fa-angle-double-down"></i>
                                    </span>
                                    <button
                                        type="button"
                                        className="btn btn-danger btn-block"
                                        id="login-btn"
                                        style={{ marginTop: 10, width: "100%" }}
                                        onClick={(e) => this.props.loginOpen(e)}
                                    >
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>

                        <span className="terms">
                            By signing up, you agree to our{" "}
                            <a
                                href="redbus.in/info/termscondition"
                                target="_blank"
                                rel="noreferrer"
                            >
                                Terms & Conditions
                            </a>
                            and{" "}
                            <a
                                href="https://www.redbus.in/info/privacypolicy"
                                target="_blank"
                                rel="noreferrer"
                            >
                                Privacy Policy
                            </a>
                        </span>
                        <button
                            className="btn close-btn"
                            onClick={(e) => this.props.signupClose(e)}
                        >
                            x
                        </button>
                        {/* <i className="fas fa-sign-in-alt"></i> */}
                    </div>
                </div>
            </div>
        );
    }
}

export default Signup;
